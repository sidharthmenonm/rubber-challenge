import jQuery from "jquery";
import slick from "slick-carousel";
import slickAnimation from './slick-animation';
import countdown from './jquery.countdown';

// $('#countdown').countdown({ until: new Date(2020, 4 - 1, 18) });

$('#countdown').countdown({
  until: new Date(2020, 4 - 1, 18),
  format: 'DHMS',
  labels: ['Years', 'Months', 'Weeks', 'Days', 'Hours', 'Mins', 'Sec'],
  labels1: ['Year', 'Month', 'Week', 'Day', 'Hour', 'Min', 'Sec'],
  layout: `
      {d<}<div class="days">{dn}<span>{dl}</span></div>{d>}
      {h<}<div class="hour">{hn}<span>{hl}</span></div>{h>}
      {m<}<div class="min">{mn}<span>{ml}</span></div>{m>}
      {s<}<div class="sec">{sn}<span>{sl}</span></div>{s>}
    `
});


jQuery('.challenges').slick({
  slidesToShow: 2,
  slidesToScroll: 1,
  autoplay: true,
  // centerMode: true,
  autoplaySpeed: 2000,

  centerPadding: '60px',
  responsive: [{
      breakpoint: 768,
      settings: {
        arrows: false,

        centerPadding: '40px',
        slidesToShow: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,

        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
}).slickAnimation();